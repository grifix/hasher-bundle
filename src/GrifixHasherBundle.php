<?php

declare(strict_types=1);

namespace Grifix\HasherBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class GrifixHasherBundle extends Bundle
{
}
