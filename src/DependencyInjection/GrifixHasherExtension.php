<?php

declare(strict_types=1);

namespace Grifix\HasherBundle\DependencyInjection;

use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

final class GrifixHasherExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $this->loadBundleConfig($configs, $container);
    }

    public function prepend(ContainerBuilder $container): void
    {
        $this->checkBundles($container);
        $this->loadConfigs($container);
    }

    private function checkBundles(ContainerBuilder $container): void
    {
        /** @var mixed[] $bundles */
        $bundles = $container->getParameter('kernel.bundles');
        if (! in_array(FrameworkBundle::class, $bundles)) {
            throw new RuntimeException('FrameworkBundle must be enabled!');
        }
    }

    private function loadConfigs(ContainerBuilder $container): void
    {
        $configDir = __DIR__ . '/../..';
        $loader    = new YamlFileLoader($container, new FileLocator($configDir));
        $loader->load('config.yaml');
    }

    /**
     * @param mixed[] $configs
     */
    private function loadBundleConfig(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);
        foreach ($config as $key => $value) {
            $container->setParameter('grifix_hasher.' . $key, $value);
        }
    }
}
