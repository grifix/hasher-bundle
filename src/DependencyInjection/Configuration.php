<?php

declare(strict_types=1);

namespace Grifix\HasherBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('grifix_hasher');
        /** @formatter:off */
        //@phpstan-ignore-next-line
        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('key')->cannotBeEmpty()
                ->end()
                ->scalarNode('algorithm')->cannotBeEmpty()
                ->end()
            ->end();
        /** @formatter:on */

        return $treeBuilder;
    }
}
