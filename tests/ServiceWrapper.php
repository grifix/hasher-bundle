<?php

declare(strict_types=1);

namespace Grifix\HasherBundle\Tests;

use Grifix\Hasher\HasherInterface;

final class ServiceWrapper
{
    public function __construct(private readonly HasherInterface $hasher)
    {
    }

    public function getHasher(): HasherInterface
    {
        return $this->hasher;
    }
}
