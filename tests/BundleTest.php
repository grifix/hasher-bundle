<?php

declare(strict_types=1);

namespace Grifix\HasherBundle\Tests;

use Grifix\HasherBundle\GrifixHasherBundle;
use Nyholm\BundleTest\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

final class BundleTest extends KernelTestCase
{
    protected static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    /**
     * @param mixed[] $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        /** @var TestKernel $kernel */
        $kernel = parent::createKernel($options);
        $kernel->addTestBundle(GrifixHasherBundle::class);
        $kernel->addTestConfig(__DIR__ . '/test_config.yaml');

        return $kernel;
    }

    public function testItWorks(): void
    {
        $kernel = self::bootKernel();

        /** @var ServiceWrapper $wrapper */
        $wrapper = $kernel->getContainer()->get(ServiceWrapper::class);

        self::assertEquals(
            '0daeeefe10b255cfbabdfbae6923cae5cdaa45a460a5f0110f1032eb1716fde5',
            $wrapper->getHasher()->hash('test'),
        );
    }
}
